public class TextPrinter
{
    public void printText(String text)
    {
        System.out.println(text);
    }

    public String underlineText(String text)
    {
StringBuilder underlineBuilder = new StringBuilder();
        for (int i = 0; i < text.length(); i++) {
            underlineBuilder.append("-");
        }
        return text + "\n" + underlineBuilder;
    }
}
