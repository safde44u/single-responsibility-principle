public class Main {

    public static void main(String[] args) {
        TextManipulator textManipulator = new TextManipulator("Mein genialer Text");
        TextPrinter printer = new TextPrinter();
        textManipulator.findWordAndReplace("genialer", "super");
        printer.printText(textManipulator.getText());
    }

}
